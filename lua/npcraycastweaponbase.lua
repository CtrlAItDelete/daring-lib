_G.DaringLib = _G.DaringLib or {}

-- Spawn SniperTrail World Effect
-- @param from_pos : userdata (Position)
-- @param to_pos : userdata (Position)
function DaringLib:set_sniper_trail(from_pos, to_pos)
	local hit_pos = Vector3()
	mvector3.set(hit_pos, to_pos)

	local idstr_trail = Idstring("trail")
	local idstr_simulator_length = Idstring("simulator_length")
	local idstr_size = Idstring("size")
	local TRAIL_EFFECT = Idstring("effects/particles/weapons/sniper_trail")
	local trail_length = World:effect_manager():get_initial_simulator_var_vector2(TRAIL_EFFECT, idstr_trail, idstr_simulator_length, idstr_size)
	local d_s = mvector3.distance_sq(from_pos, to_pos)


	local trail = World:effect_manager():spawn({
		effect = Idstring("effects/particles/weapons/sniper_trail"),
		position = from_pos,
		normal = hit_pos - from_pos
	})

	mvector3.set_y(trail_length, math.sqrt(d_s))
	World:effect_manager():set_simulator_var_vector2(trail, idstr_trail, idstr_simulator_length, idstr_size, trail_length)
end