_G.DaringLib = _G.DaringLib or {}

-- Return player's camera position
-- @return number
function DaringLib:camera_position()
	local camera = managers.player:player_unit():camera()
	local position = camera:position()
	
	return position
end

-- Return player's camera rotation
-- @return number
function DaringLib:camera_rotation()
	local camera = managers.player:player_unit():camera()
	local rotation = camera:rotation()
	
	return rotation
end

-- Return the position of the center point of the player's camera
-- maxRaycastDis : number
-- @return userdata
function DaringLib:camera_center_position(maxRaycastDis)
	local player_unit = managers.player:player_unit()
	
	if player_unit then
		local maxRaycastDis = maxRaycastDis or 50000
		local from = self:camera_position()
		local to = from + self:camera_rotation():y() * maxRaycastDis
		local raycast = World:raycast("ray", from, to)

		return raycast and raycast.position or to
	end
end

-- Return the enemy of the center point of the player's camera
-- maxRaycastDis : number
-- @return table
function DaringLib:camera_center_enemy(maxRaycastDis)
	local player_unit = managers.player:player_unit()
	
	if player_unit then
		local maxRaycastDis = maxRaycastDis or 50000
		local from = self:camera_position()
		local to = from + self:camera_rotation():y() * maxRaycastDis
		local raycast = World:raycast("ray", from, to, "slot_mask", managers.slot:get_mask("enemies", "bullet_blank_impact_targets"))
		
		if raycast and alive(raycast.unit) and managers.enemy:is_enemy(raycast.unit) then
			return raycast.unit
		end
	end
end