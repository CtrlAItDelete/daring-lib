-- // IntraUI var0.03 | by MetroLine
IntraUI = IntraUI or class()
local save_path = SavePath

function IntraUI:init(data)
	self._data = data
	self._ws = managers.gui_data:create_fullscreen_workspace()
	self._visible = false
	self._alpha = 0
	self._w = data.w or 300
	self._h = data.h or 500
	self._panel = self._ws:panel():panel({
		name = "IntraUI",
		alpha = self._alpha,
		visible = self._visible
	})

	self._menu = self._panel:panel({
		name = "main_menu",
		visible = true
	})

	self._bg = self._menu:bitmap({
		name = "ui_background",
		vertical = "grow",
		align = "grow",
		halign = "grow",
		visible = true,
		render_template = "VertexColorTexturedBlur3D",
		texture = "guis/textures/test_blur_df",
		layer = 100,
		w = self._w,
		h = self._h,
		x = self._panel:center_x() / 2,
		y = self._panel:center_y() / 2,
		color = Color.white
	})
	self._bg:set_center(self._panel:w() / 2, self._panel:h() / 2)

	self._sl_top = self._menu:bitmap({
		name = "shared_lines_top",
		texture = "guis/textures/pd2/shared_lines",
		layer = 101,
		w = self._w,
		h = 5,
		x = self._bg:left(),
		y = self._bg:top()
	})
	
	self._sl_buttom = self._menu:bitmap({
		name = "shared_lines_buttom",
		texture = "guis/textures/pd2/shared_lines",
		layer = 101,
		w = self._w+5,
		h = 5,
		x = self._bg:left(),
		y = self._bg:bottom()
	})
	
	self._sl_left = self._menu:bitmap({
		name = "shared_lines_left",
		texture = "guis/textures/pd2/shared_lines",
		layer = 101,
		w = 5,
		h = self._h,
		x = self._bg:left(),
		y = self._bg:top()
	})
	
	self._sl_right = self._menu:bitmap({
		name = "shared_lines_left",
		texture = "guis/textures/pd2/shared_lines",
		layer = 101,
		w = 5,
		h = self._h,
		x = self._bg:right(),
		y = self._bg:top()
	})
	
	self._none_button = {}
	for i = 1, 5 do
		self._none_button[i] = self._menu:bitmap({
			name = "none_button"..tostring(i),
			visible = i == 1 and true or false,
			texture = "guis/textures/pd2/none_icon",
			layer = 101,
			alpha = 1,
			color = Color.white,
			w = 100,
			h = 100,
			x = self._sl_top:right()-20,
			y = self._sl_top:top()-20
		})	
	end
	
	self._none_button_bg = self._menu:bitmap({
		name = "none_button_bg",
		visible = true,
		vertical = "bottom",
		align = "top",
		halign = "top",
		texture = "guis/textures/menu_selected",
		texture_rect = {20, 20, 24, 24},
		color = Color.white,
		alpha = 0,
		layer = 101,
		w = 50,
		h = 50
	})
	self._none_button_bg:set_center(self._none_button[1]:center_x()+0.5, self._none_button[1]:center_y())
	
	self._title_top = {}
	self._all_title_data = self:all_title_data()
	
	self._tickbox_bg = {}
	self._tickbox_toggle = {}
	self._tickbox_text = {}
	self._button_bg = {}
	self._button_text = {}
	self._slider_bg = {}
	self._slider_box = {}
	self._slider_text = {}
	self._slider_state = {}
	self._state = {}
	self._all_ui_data = self:all_ui_data()	
	self._ui = {}
	self._child = {}
	self._current_ui = data.main_menu
	self._menu_layer = {}
	self._current_layer = {}
	self._queue_layer = {}
	self._queue_layer[1] = data.main_menu
	self._all_ui = {}
	
	local TITLE
	for key, menu in pairs(self._all_ui_data) do
		if key then
			self._menu_layer[key] = self._menu_layer[key] or 1
			self._ui[key] = self._ui[key] or self._menu:panel({
				name = key,
				visible = data.main_menu == key
			})
			
			if self._all_title_data[key] then
				for _, title in pairs(self._all_title_data[key]) do
					TITLE = title
				end
			end
			
			self._title_top[key] = self._ui[key]:text({
				name = tostring(key),
				color = menu.color or Color.white,
				font = tweak_data.hud_players.ammo_font,
				text = TITLE,
				alpha = 102,
				font_size = 30,
				layer = 102
			})

			self._title_top[key]:set_align("center")
			self._title_top[key]:set_vertical("center")
			self._title_top[key]:set_center(self._bg:center_x(), self._bg:top() + 25)
			
			for _, ui in pairs(menu) do
				if ui.type == "toggle" then
					self:add_toggle(key, ui)
				elseif ui.type == "button" then
					self:add_button(key, ui)
				elseif ui.type == "slider" then
					self:add_slider(key, ui)
				elseif ui.type == "choice" then
					self:add_choice(key, ui)
				end
			end
		end
	end

	self._slider_bgbox = {}
	for _, box in pairs(self._slider_bg) do
		table.insert(self._slider_bgbox, #self._slider_bgbox+1, box)
	end
	
	self._ws:connect_keyboard(Input:keyboard())
	self._panel:key_press(callback(self, self, "key_press"))
	self._panel:key_release(callback(self, self, "key_release"))	
	Hooks:Add("MenuUpdate", "CustomUIUpdate_Menu"..tostring(self), callback(self, self, "update"))
	Hooks:Add("GameSetupUpdate", "CustomUIUpdate_GameSetup"..tostring(self), callback(self, self, "update"))
	self._save_path = save_path .. data.name.. ".json"
	self:load_json(self._save_path)
	if data.keybind then
		Input:keyboard():add_trigger(Idstring(data.keybind), callback(self, self, "show"))
	end
end

function IntraUI:add_toggle(menu_name, ui)
	self._state[ui.name] = {}
	self._state[ui.name].type = ui.type
	self._state[ui.name].state = ui.state or false
	
	local size = ui.size and ui.size
	self._tickbox_bg[ui.name] = self._ui[menu_name]:bitmap({
		name = ui.name,
		visible = false,
		vertical = "center",
		align = "center",
		halign = "center",
		texture = "guis/textures/menu_selected",
		texture_rect = {20, 20, 24, 24},
		color = Color.white,
		alpha = 0.3,
		layer = 101,
		w = size and ((#ui.text * size) / 3) + (ui.bg_w and ui.bg_w or 100) or #ui.text + (ui.bg_w and ui.bg_w or 100),
		h = size or 22 ,
		x = self._bg:left()+ui.x,
		y = self._bg:top()+ui.y
	})
			
	self._tickbox_toggle[ui.name] = self._ui[menu_name]:bitmap({
		name = ui.name and ui.name .. "[tickbox_toggle]" or nil,
		texture = "guis/textures/menu_tickbox",
		texture_rect = {
			self._state[ui.name].state and 24 or 0,
			0,
			24,
			24
		},
		w = size or 20,
		h = size or 20,
		layer = 102,
		x = self._tickbox_bg[ui.name]:left(),
		y = self._tickbox_bg[ui.name]:top()
	})
						
	self._tickbox_text[ui.name] = self._ui[menu_name]:text({
		name = ui.name .. "[tickbox_text]",
		color = ui.color or Color.white,
		vertical = "top",
		font = tweak_data.hud_players.ammo_font,
		text = ui.text,
		alpha = 102,
		font_size = size and size - 5 or 15,
		layer = 102,
		x = self._tickbox_toggle[ui.name]:right(),
		y = self._tickbox_bg[ui.name]:top() + self._tickbox_bg[ui.name]:h() / 7.5
	})
	table.insert(self._child, #self._child+1, self._tickbox_bg[ui.name])
	table.insert(self._child, #self._child+1, self._tickbox_toggle[ui.name])
	table.insert(self._child, #self._child+1, self._tickbox_text[ui.name])
	table.insert(self._all_ui, self._tickbox_bg[ui.name])
end

function IntraUI:add_button(menu_name, ui)
	local size = ui.size or 22
	self._state[ui.name] = {}
	self._state[ui.name].type = ui.type
	self._button_bg[ui.name] = self._ui[menu_name]:bitmap({
		name = ui.name,
		visible = false,
		vertical = "center",
		align = "center",
		halign = "center",
		texture = "guis/textures/menu_selected",
		texture_rect = {20, 20, 24, 24},
		color = Color.white,
		alpha = 0.3,
		layer = 101,
		w = ui.bg_w and (ui.bg_w * (size or 25)) or (#ui.text * (size or 25) / 4),
		h = size,
		x = self._bg:left()+ui.x,
		y = self._bg:top()+ui.y
	})
			
	self._button_text[ui.name] = self._ui[menu_name]:text({
		name = ui.name .. "[button_text]",
		color = ui.color or Color.white,
		vertical = "top",
		font = tweak_data.hud_players.ammo_font,
		text = ui.text,
		alpha = 102,
		font_size = size and size - 5 or 20,
		layer = 102,
		x = self._button_bg[ui.name]:left()+2,
		y = self._button_bg[ui.name]:top() + self._button_bg[ui.name]:h() / 7.5
	})
	table.insert(self._child, #self._child+1, self._button_bg[ui.name])
	table.insert(self._child, #self._child+1, self._button_text[ui.name])
	table.insert(self._all_ui, self._button_bg[ui.name])
end

function IntraUI:add_slider(menu_name, ui)
	local state = ui.state or 0
	local w = ui.w or 200
	self._state[ui.name] = {}
	self._state[ui.name].type = ui.type
	self._state[ui.name].state = state
	self._state[ui.name].is_click = false
	self._slider_bg[ui.name] = self._ui[menu_name]:bitmap({
		name = ui.name,
		visible = false,
		vertical = "center",
		align = "center",
		halign = "center",
		texture = "guis/textures/menu_selected",
		texture_rect = {20, 20, 24, 24},
		color = Color.white,
		alpha = 0.6,
		layer = 101,
		w = w or 100,
		h = 22 ,
		x = self._bg:left()+ui.x,
		y = self._bg:top()+ui.y
	})
	
	self._slider_box[ui.name] = self._ui[menu_name]:bitmap({
		name = ui.name .."[slider_box]",
		visible = true,
		vertical = "center",
		align = "center",
		halign = "center",
		texture = "guis/textures/menu_selected",
		texture_rect = {20, 20, 24, 24},
		color = Color.white,
		alpha = 0.6,
		layer = 101,
		w = w / ui.max * state,
		h = 22 ,
		x = self._bg:left()+ui.x,
		y = self._bg:top()+ui.y
	})
			
	self._slider_state[ui.name] = self._ui[menu_name]:text({
		name = ui.name .. "[slider_state]",
		color = ui.color or Color.white,
		font = tweak_data.hud_players.ammo_font,
		vertical = "center",
		align == "center",
		text = tostring(state),
		alpha = 102,
		font_size = 20,
		layer = 102,
	})
	self._slider_state[ui.name]:set_align("center")
	self._slider_state[ui.name]:set_vertical("center")
	self._slider_state[ui.name]:set_center(self._slider_bg[ui.name]:center())

	self._slider_text[ui.name] = self._ui[menu_name]:text({
		name = ui.name .. "[slider_text]",
		color = ui.color or Color.white,
		font = tweak_data.hud_players.ammo_font,
		text = ui.text,
		alpha = 102,
		font_size = 15,
		layer = 102,
		x = self._slider_bg[ui.name]:left()+2,
		y = self._slider_bg[ui.name]:top() + self._slider_bg[ui.name]:h() / 7
	})
	table.insert(self._child, #self._child+1, self._slider_bg[ui.name])
	table.insert(self._child, #self._child+1, self._slider_box[ui.name])
	table.insert(self._child, #self._child+1, self._slider_state[ui.name])
	table.insert(self._child, #self._child+1, self._slider_text[ui.name])
	table.insert(self._all_ui, self._slider_bg[ui.name])
end

function IntraUI:mouse_moved(o, x, y)
	if not self._visible then
		return
	end

	self._mouse_inside = false
	for _, box in pairs(self._all_ui) do
		if self._ui[self:current_layer()] and self._ui[self:current_layer()]:child(box:name()) then
			if box:inside(x, y) then
				if self:get_type(box:name()) ~= "slider" then
					box:set_visible(true)
				end
					
				self._mouse_inside = true
				break
			else
				if self:get_type(box:name()) ~= "slider" then
					box:set_visible(false)
				end
			end
		end
	end
	
	if self._none_button_bg:inside(x, y) then
		self._none_button_bg:set_visible(false)
		self._mouse_inside = true
		self:none_button_visible(true)
	else
		self._none_button_bg:set_visible(false)
		self:none_button_visible(false)
	end
	
	if self._mouse_inside then
		managers.mouse_pointer:set_pointer_image("link")
	else
		managers.mouse_pointer:set_pointer_image("arrow")
	end
	
	if self._clicked_pos then
		self._clicked_pos.x_move = self._clicked_pos.x - x
		self._clicked_pos.y_move = self._clicked_pos.y - y
		
		if self._clicked_bg then
			local clicked_ui = self:get_ui(self._clicked_slider)
			local ui_data = self:get_ui_data(self._clicked_bg)
			local box_w = clicked_ui:w()
			local max_state =ui_data.max
			local min_state = ui_data.min
			local max_w = ui_data.w
			if box_w <= max_w then
				w = self._slider_w - self._clicked_pos.x_move
			end
			
			if w >= max_w then
				w = max_w
			elseif w <= min_state then
				w = min_state
			end
			
			clicked_ui:set_w(w)
			state = max_state * w / max_w
			state = math.round(state)
			self._state[self._clicked_bg].state = state
			self._slider_state[self._clicked_bg]:set_text(tostring(state))
			self:func_callback(self._clicked_bg)
		end
	end
end

function IntraUI:mouse_pressed(o, button, x, y)
	self._clicked_pos = {}
	self._clicked_pos.x = x
	self._clicked_pos.y = y
	
	for _, slider in pairs(self._slider_bgbox) do
		if slider:inside(x, y) then
			if self._ui[self:current_layer()] and self._ui[self:current_layer()]:child(slider:name()) then
				self._clicked_bg = slider:name()
				self._clicked_slider = slider:name() .. "[slider_box]"
				self._slider_w = self._slider_box[self._clicked_bg]:w()
				self._state[slider:name()].is_clicked = true
				break
			end
		end
	end
end

function IntraUI:mouse_released(o, button, x, y)
	if self._clicked_pos then
		self._clicked_pos = nil
		self._clicked_bg = nil
		self._clicked_slider = nil
		self._slider_w = nil
	end
end

function IntraUI:mouse_clicked(o, button, x, y)
	for _, box in pairs(self._all_ui) do
		if box:inside(x, y) then
			if self._ui[self:current_layer()] and self._ui[self:current_layer()]:child(box:name()) then
				if self:get_type(box:name()) == "toggle" then
					self:set_state(box:name(), not self:get_state(box:name()))
					self:get_ui(box:name() .. "[tickbox_toggle]"):set_texture_rect(
						self._state[box:name()].state and 24 or 0,
						0,
						24,
						24
					)
					self:func_callback(box:name())
				elseif self:get_type(box:name()) == "button" then
					self:func_callback(box:name())
					if self:get_ui_data(box:name()).menu then
						self:switch_layer(self:get_ui_data(box:name()).menu.name, 1)
					end
				elseif self:get_type(box:name()) == "slider" then
					-- Noting
				end
			end
		end
	end
	
	if self._none_button_bg:inside(x, y) then
		self:hide()
	end
end

function IntraUI:show()
	if self._visible then
		return
	end

	self._visible = true
	self._panel:set_visible(self._visible)
		
	if game_state_machine then
		game_state_machine:current_state():set_controller_enabled(not managers.player:player_unit())  --锁定玩家视角
	end
		
	local active_menu = managers.menu:active_menu()
	local is_pc_controller = managers.menu:is_pc_controller()
	if active_menu and not is_pc_controller then
		active_menu.input:activate_controller_mouse()
	end
	
	self._mouse_id = self._mouse_id or managers.mouse_pointer:get_id()
	self._mouse_data = {
		mouse_move = callback(self, self, "mouse_moved"),
		mouse_press = callback(self, self, "mouse_pressed"),
		mouse_release = callback(self, self, "mouse_released"),
		mouse_click = callback(self, self, "mouse_clicked"),
		id = self._mouse_id,
		menu_ui_object = self
	}
	managers.mouse_pointer:use_mouse(self._mouse_data)  --显示鼠标
end

function IntraUI:hide()
	if not self._visible then
		return
	end
	
	managers.mouse_pointer:remove_mouse(self._mouse_data)
	self._visible = false
	
	if self._panel:alpha() == 0 then
		self._panel:set_visible(self._visible)
	end
	
    if game_state_machine then
      game_state_machine:current_state():set_controller_enabled(true)
    end
	
	local active_menu = managers.menu:active_menu()
	local is_pc_controller = managers.menu:is_pc_controller()
	if active_menu and not is_pc_controller then
		active_menu.input:activate_controller_mouse()
	end
	
	for _, box in pairs(self._all_ui) do
		if self:get_type(box:name()) ~= "slider" then
			box:set_visible(false)
		end
	end
	
	self:none_button_visible(false)
	self:save_json(self._save_path)
end

function IntraUI:update(t, dt)
	if not self._panel:visible() then
		return
	end

	if self._visible then
		self._alpha = self._panel:alpha() < 1 and self._panel:alpha() + dt*3 or 1
	else
		self._alpha = self._panel:alpha() > 0 and self._panel:alpha() - dt*3 or 0
	end
	
	self._panel:set_alpha(self._alpha)
	
	if self._panel:alpha() == 0 then
		self._panel:set_visible(self._visible)
	end
	
	for _, text in pairs(self._tickbox_text) do
		text:set_alpha(self._alpha)
	end
	
	for _, text in pairs(self._button_text) do
		text:set_alpha(self._alpha)
	end
	
	for _, text in pairs(self._slider_state) do
		text:set_alpha(self._alpha)
	end	
	
	for _, text in pairs(self._slider_text) do
		text:set_alpha(self._alpha)
	end	
	
	for _, text in pairs(self._title_top) do
		text:set_alpha(self._alpha)
	end
end

function IntraUI:key_press(o, k)
end

function IntraUI:key_release(o, k)
	if k == Idstring("esc") then
		if self._menu_layer[self._current_ui] == 1 then
			self:hide()
		else
			self:switch_layer(self._queue_layer[self._menu_layer[self._current_ui]-1], -1)
		end
	end
end

function IntraUI:all_title_data()
	local _all_title = {}
	
	for _, menu in pairs(self._data) do
		if type(menu) == "table" then
			if menu.title then
				_all_title[menu.name] = {}
				table.insert(_all_title[menu.name], #_all_title[menu.name]+1, menu.title)
			end
		end
	end
	
	return _all_title
end

function IntraUI:all_ui()
	local _all_ui = {}
	
	for _, box in pairs(self._tickbox_bg) do
		table.insert(_all_ui, #_all_ui+1, box)
	end
	
	for _, box in pairs(self._button_bg) do
		table.insert(_all_ui, #_all_ui+1, box)
	end

	for _, box in pairs(self._slider_bg) do
		table.insert(_all_ui, #_all_ui+1, box)
	end	
	
	return _all_ui
end

function IntraUI:all_ui_data()
	local _all_ui = {}
	
	local function insert_all_data_ui(menu)
		if menu then
			_all_ui[menu.name] = {}
			for _, box in pairs(menu) do
				if type(box) == "table" then
					table.insert(_all_ui[menu.name], #_all_ui[menu.name]+1, box)
				end
			end
			
			for _, ui in pairs(menu) do
				if type(ui) == "table" then
					if ui.menu then
						insert_all_data_ui(ui.menu)
					end
				end
			end
		end
	end
	
	for _, menu in pairs(self._data) do
		if type(menu) == "table" then
			_all_ui[menu.name] = {}
			for _, box in pairs(menu) do
				if type(box) == "table" then
					table.insert(_all_ui[menu.name], #_all_ui[menu.name]+1, box)
					if box.menu then
						insert_all_data_ui(box.menu)
					end
				end
			end
		end
	end
	
	return _all_ui
end

function IntraUI:get_ui_data(name)
	for _, menu in pairs(self._data) do
		if type(menu) == "table" then
			for _, ui in pairs(menu) do
				if ui.name == name then
					return ui
				end
			end
		end
	end
	
	for _, menu in pairs(self._all_ui_data) do
		for _, ui in pairs(menu) do
			if ui.name == name then
				return ui
			end
		end
	end
end

function IntraUI:get_ui(name)
	if not name then
		return
	end

	for _, child in pairs(self._child) do
		if child:name() == name then
			return child
		end
	end
end

function IntraUI:get_state(name)
	if not name then
		return
	end

	return self._state[name].state
end

function IntraUI:set_state(name, state)
	if not name then
		return
	end
	
	if self._state[name].type == "toggle" then
		self._state[name].state = state or false
	elseif self._state[name].type == "slider" then
		self._state[name].state = state or 0
	end
	
	self:set_ui_state(name, state)
end

function IntraUI:set_ui_state(name, state)
	if not name then
		return
	end
	
	if self._state[name].type == "toggle" then
		local ui = self:get_ui(name .. "[tickbox_toggle]")
		ui:set_texture_rect(
			state and 24 or 0,
			0,
			24,
			24
		)
	elseif self._state[name].type == "slider" then
		local ui = self:get_ui(name .. "[slider_box]")
		local ui_data = self:get_ui_data(name)
		local max_w = ui_data.w
		local max_state = ui_data.max
		ui:set_w(state * max_w / max_state)
		
		self._slider_state[name]:set_text(state)
	end
end

function IntraUI:get_type(name)
	if not name then
		return
	end
	
	return self._state[name].type
end

function IntraUI:func_callback(name)
	local data_ui = self:get_ui_data(name)
	local callback_function = data_ui.callback
	if callback_function then
		local parameter = self._state[name].state
		callback_function(data_ui.id or parameter)
	end
end

function IntraUI:none_button_visible(visible)
	for i = 2, 5 do
		self._none_button[i]:set_visible(visible)
	end
end

function IntraUI:save_json(path)
	self:set_status()
	
	local file = io.open(path, "w+")
	if file then
		file:write(json.encode(self._status))
		file:close()
	end
end

function IntraUI:load_json(path)
	local file = io.open(path, "r")
	if file then
		local options = json.decode(file:read("*all"))
		for key, ops in pairs(options) do
			if self._state[key] then
				self._state[key].state = ops
				self:set_state(key, ops)
			end
		end
		file:close()
	end
end

function IntraUI:set_status()
	self._status = {}
	
	for _, ui in pairs(self._all_ui) do
		self._status[ui:name()] = self._state[ui:name()].state
	end
end

function IntraUI:add_ui(menu_name, data)
	if not data then
		return
	end

	table.insert(self._all_ui_data[menu_name], data)
	if data.type == "toggle" then
		self:add_toggle(menu_name, data)
	elseif data.type == "button" then
		self:add_button(menu_name, data)
	elseif data.type == "slider" then
		self:add_slider(menu_name, data)
	end
end

function IntraUI:switch_menu(menu_name)
	self._queue_layer = {}
	self._queue_layer[1] = menu_name
	for _, menu in pairs(self._ui) do
		if menu:name() == menu_name then
			menu:set_visible(true)
			self._current_ui = menu_name
		else
			menu:set_visible(false)
		end
	end
end

function IntraUI:switch_layer(menu_name, layer)
	for _, menu in pairs(self._ui) do
		if menu:name() == menu_name then
			menu:set_visible(true)
		else
			menu:set_visible(false)
		end
	end
	
	self._menu_layer[self._current_ui] = self._menu_layer[self._current_ui] + layer
	self._queue_layer[self._menu_layer[self._current_ui]] = menu_name
end

function IntraUI:current_layer()
	return self._queue_layer[self._menu_layer[self._current_ui]]
end
