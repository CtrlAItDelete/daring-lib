#AutoMenuBuilder
For AutoMenuBuilder function, check https://www.modworkshop.net/mod/29982


#DaringLib

·DaringLib:play_audio(unit, path)
Ask 'unit' to play an audio file from 'path' at any time.
if 'unit' doesn't exist, the audio will be played by you.

#Player Manager

·DaringLib:get_my_game_id()
Return your game id.

·DaringLib:get_my_steam_id()
Return your steam id.

·DaringLib:get_local_player()
Return your player table.

·DaringLib:get_local_player_unit()
Return your player unit table.

·DaringLib:get_local_player_position(unit_name)
Return your position for local player unit.
> unit_name : "player" | "head" | nil

·DaringLib:get_local_player_rotation()
Return your rotation for local player unit.

·DaringLib:get_local_player_weapon_id()
Return the ID of the currently held weapon.

·DaringLib:get_local_player_weapon_type()
Return the type of the currently held weapon.

#Player Camera

·DaringLib:camera_position()
Return player's camera position.

·DaringLib:camera_rotation()
Return player's camera rotation.

·DaringLib:camera_center_position(maxRaycastDis)
Return the position of the center point of the player's camera.
> maxRaycastDis : number

·DaringLib:camera_center_enemy(maxRaycastDis)
Return the enemy of the center point of the player's camera.
> maxRaycastDis : number

#Network

·DaringLib:get_peers()
Return all peers(retun a table).

·DaringLib:get_peers_and_local_peer()
Return all peers and local peer(retun a table).

·DaringLib:get_peer_by_id(peer_id)
Use 'peer_id' to get the peer(retun a table).
> peer_id : number

·DaringLib:get_peer_level(peer_id)
Use 'peer_id' to get the player Level.
> peer_id : number

·DaringLib:get_peer_rank(peer_id)
Use 'peer_id' to get the player Rank level.
> peer_id : number

·DaringLib:get_peer_platform(peer_id)
Use 'peer_id' to get the player Platform.
> peer_id : number

·DaringLib:get_peer_mods(peer_id)
Use 'peer_id' to get the player mods list(retun a table).
> peer_id : number

·DaringLib:get_peer_ping(peer_id)
Use 'peer_id' to get the player ping.
> peer_id : number

·DaringLib:get_peer_position(unit_name)
Return peer's position.
> unit_name : "player" | "head" | nil

·DaringLib:get_peer_rotation()
Return peer's rotation.

#Enemy

·DaringLib:get_enemies_amount()
Return the current amount of enemies on the field
- @return number

·DaringLib:is_enemy(unit)
Return wether the unit is a enemy
- @return boolean

·DaringLib:is_civilian(unit)
Return wether the unit is a civilian
- @return boolean

·DaringLib:get_enemy_type(unit)
Return enemy type("taser" "tank" ...)
- @return string

·DaringLib:get_enemy_weapon_id(unit)
Return Weapon ID used by the enemy
- @return string

·DaringLib:get_enemy_distance(ene_unit, to_unit)
Return the distance between the enemy and you
> ene_unit : userdata(unit)
> to_unit : userdata(unit)
- @return number

·DaringLib:get_enemy_health(unit)
Return enemy health
- @return number

·DaringLib:chk_surrender(unit)
Check enemy can be surrender
- @return boolean

·DaringLib:get_nearby_enemies(position, distance)
Return units with a distance less than 'distance' from the position 'position'
> position : userdata (if 'nil', auto pass in "player_unit")
> distance : number (if 'nil', auto pass in 800)
- @return a table(enemies)

#World Effect

·DaringLib:set_sniper_trail(from_pos, to_pos)
The "Sniper Trail Effect" of spawn from 'from_pos' to' to_pos'

#Other functions
·logtbl(table)
Print a table in BLT log. 
